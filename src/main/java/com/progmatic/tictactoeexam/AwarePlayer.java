/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Áron
 */
public class AwarePlayer extends AbstractPlayer {

    public AwarePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        int rowCounter;
        int columnCounter;
        int diagonalCounter;
        Cell lastEmptyCol = new Cell(0, 0, PlayerType.EMPTY);
        Cell lastEmptyRow = new Cell(0, 0, PlayerType.EMPTY);;
        Cell lastEmptyDiag = new Cell(0, 0, PlayerType.EMPTY);
        List<Cell> table;
        if (b instanceof Table) {
            table = ((Table) b).getTable();
            for (int i = 0; i < 3; i++) {
                columnCounter = 0;
                rowCounter = 0;
                diagonalCounter = 0;
                for (Cell cell : table) {
                    if (cell.getCol() == i && cell.getCellsPlayer() == myType) {
                        columnCounter++;
                    }
                    if (cell.getCol() == i && cell.getCellsPlayer() == PlayerType.EMPTY) {
                        lastEmptyCol = new Cell(cell.getRow(), cell.getCol(), cell.getCellsPlayer());
                    }
                    if (cell.getRow() == i && cell.getCellsPlayer() == myType) {
                        rowCounter++;
                    }
                    if (cell.getRow() == i && cell.getCellsPlayer() == PlayerType.EMPTY) {
                        lastEmptyRow = new Cell(cell.getRow(), cell.getCol(), cell.getCellsPlayer());
                    }
                    if (i == 0) {
                        if (cell.getRow() == cell.getCol() && cell.getCellsPlayer() == myType) {
                            diagonalCounter++;
                        }
                        if (cell.getRow() == cell.getCol() && cell.getCellsPlayer() == PlayerType.EMPTY) {
                            lastEmptyDiag = new Cell(cell.getRow(), cell.getCol(), cell.getCellsPlayer());
                        }
                    }
                    if (i == 2) {
                        if (cell.getRow() + cell.getCol() == 2 && cell.getCellsPlayer() == myType) {
                            diagonalCounter++;
                        }
                        if (cell.getRow() == cell.getCol() && cell.getCellsPlayer() == PlayerType.EMPTY) {
                            lastEmptyDiag = new Cell(cell.getRow(), cell.getCol(), cell.getCellsPlayer());
                        }
                    }
                    if (columnCounter == 2) {
                        lastEmptyCol.setCellsPlayer(myType);
                        return lastEmptyCol;
                    }
                    if (rowCounter == 2) {
                        lastEmptyRow.setCellsPlayer(myType);
                        return lastEmptyRow;
                    }
                    if (diagonalCounter == 2) {
                        lastEmptyDiag.setCellsPlayer(myType);
                        return lastEmptyDiag;
                    }
                }
            }
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    for (Cell cell : table) {
                        if (cell.getRow() == i && cell.getCol() == j && cell.getCellsPlayer() == PlayerType.EMPTY) {
                            Cell cellForward = new Cell(cell.getRow(), cell.getCol(), myType);
                            return cellForward;
                        }
                    }
                }
            }
            return null;
        }
        return null;
    }
//    @Override
//    public Cell nextMove(Board b) {
//        List<Cell> table;
//        if (b instanceof Table) {
//            table = ((Table) b).getTable();
//            for (int i = 0; i < 3; i++) {
//                for (int j = 0; j < 3; j++) {
//                    for (Cell cell : table) {
//                        if (cell.getRow() == i && cell.getCol() == j && cell.getCellsPlayer() == PlayerType.EMPTY) {
//                            Cell cellForward = new Cell(cell.getRow(), cell.getCol(), myType);
//                            return cellForward;
//                        }
//                    }
//                }
//            }
//            return null;
//        }
//        return null;
//    }
}
