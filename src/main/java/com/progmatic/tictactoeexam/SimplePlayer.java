/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Áron
 */
public class SimplePlayer extends com.progmatic.tictactoeexam.interfaces.AbstractPlayer {

    public SimplePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        List<Cell> table;
        if (b instanceof Table) {
            table = ((Table) b).getTable();
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    for (Cell cell : table) {
                        if (cell.getRow() == i && cell.getCol() == j && cell.getCellsPlayer() == PlayerType.EMPTY) {
                            Cell cellForward = new Cell(cell.getRow(), cell.getCol(), myType);
                            return cellForward;
                        }
                    }
                }
            }
            return null;
        }
        return null;
    }
}
