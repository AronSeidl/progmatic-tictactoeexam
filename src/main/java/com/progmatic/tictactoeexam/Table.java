/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Áron
 */
public class Table implements com.progmatic.tictactoeexam.interfaces.Board {

    private List<Cell> table = new ArrayList<>();
    private static final int MAXCOMBONUMBER = 3;

    public Table() {
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                Cell cell = new Cell(row, column);
                table.add(cell);
            }
        }

    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {

        for (Cell cell : table) {
            if (cell.getRow() == rowIdx && cell.getCol() == colIdx) {
                return cell.getCellsPlayer();
            }
        }
        throw new CellException(rowIdx, colIdx, "Ilyen mező nincs a táblán!");

    }

    @Override
    public void put(Cell cell) throws CellException {

        if (cell.getRow() > 2
                || cell.getRow() < 0
                || cell.getCol() > 2
                || cell.getCol() < 0) {
            throw new CellException(cell.getRow(), cell.getCol(), "Érvénytelen mező!");
        }

        for (Cell tableCell : table) {
            if (tableCell.getRow() == cell.getRow() && tableCell.getCol() == cell.getCol() && tableCell.getCellsPlayer() != PlayerType.EMPTY) {
                throw new CellException(cell.getRow(), cell.getCol(), "A mező már foglalt!");
            }
            if (tableCell.getRow() == cell.getRow() && tableCell.getCol() == cell.getCol() && tableCell.getCellsPlayer() == PlayerType.EMPTY) {
                tableCell.setCellsPlayer(cell.getCellsPlayer());
            }
        }
//            Iterator<Cell> tableIt = table.iterator();
//            while (tableIt.hasNext()) {
//                Cell tableCell = tableIt.next();
//                if (tableCell.getRow() == cell.getRow() && tableCell.getCol() == cell.getCol()) {
//                    tableIt.remove();
//                    table.add(cell);
//                }
//            }

    }

    @Override
    public boolean hasWon(PlayerType p) {
        int rowCounter;
        int columnCounter;
        int diagonalCounter;
        List<Cell> cellsOfPlayer = new ArrayList<>();
        for (Cell cell : table) {
            if (cell.getCellsPlayer() == p) {
                cellsOfPlayer.add(cell);
            }
        }
        if (cellsOfPlayer.size() < 3) {
            return false;
        } else if (cellsOfPlayer.size() > 5) {
            return true;
        } else {
            for (int i = 0; i < 3; i++) {
                columnCounter = 0;
                rowCounter = 0;
                diagonalCounter = 0;
                for (Cell cell : cellsOfPlayer) {
                    if (cell.getCol() == i && cell.getCellsPlayer() == p) {
                        columnCounter++;
                    }
                    if (cell.getRow() == i && cell.getCellsPlayer() == p) {
                        rowCounter++;
                    }
                    if (i == 0) {
                        if (cell.getRow() == cell.getCol() && cell.getCellsPlayer() == p) {
                            diagonalCounter++;
                        }
                    } else if (i == 2) {
                        if (cell.getRow() + cell.getCol() == 2 && cell.getCellsPlayer() == p) {
                            diagonalCounter++;
                        }
                    }
                    if (columnCounter == MAXCOMBONUMBER || rowCounter == MAXCOMBONUMBER || diagonalCounter == MAXCOMBONUMBER) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> emptyCells = new ArrayList<>();
        for (Cell cell : table) {
            if (cell.getCellsPlayer() == PlayerType.EMPTY) {
                emptyCells.add(cell);
            }
        }
        return emptyCells;
    }

    public List<Cell> getTable() {
        return table;
    }

}
